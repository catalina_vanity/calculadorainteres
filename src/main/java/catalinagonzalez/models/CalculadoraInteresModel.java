/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package catalinagonzalez.models;

/**
 *
 * @author catal
 */
public class CalculadoraInteresModel {
    public CalculadoraInteresModel(double capital,int aniosPlazo,double tasaInteresAnual)
    {
        this.capital=capital;
        this.aniosPlazo=aniosPlazo;
        this.tasaInteresAnual=tasaInteresAnual;
        this.interesSimple=0;
    }
    
    private double capital;
    private int aniosPlazo;
    private double tasaInteresAnual;
    private double interesSimple;
    
     /**
     * @return the capital
     */
    public double getCapital() {
        return capital;
    }

    /**
     * @param capital the capital to set
     */
    public void setCapital(double capital) {
        this.capital = capital;
    }

    /**
     * @return the aniosPlazo
     */
    public int getAniosPlazo() {
        return aniosPlazo;
    }

    /**
     * @param aniosPlazo the aniosPlazo to set
     */
    public void setAniosPlazo(int aniosPlazo) {
        this.aniosPlazo = aniosPlazo;
    }

    /**
     * @return the tasaInteresAnual
     */
    public double getTasaInteresAnual() {
        return tasaInteresAnual;
    }

    /**
     * @param tasaInteresAnual the tasaInteresAnual to set
     */
    public void setTasaInteresAnual(double tasaInteresAnual) {
        this.tasaInteresAnual = tasaInteresAnual;
    }

    /**
     * @return the interesSimple
     */
    public double getInteresSimple() {
        return interesSimple;
    }

    /**
     * @param interesSimple the interesSimple to set
     */
    public void setInteresSimple(double interesSimple) {
        this.interesSimple = interesSimple;
    }
    
    public double calculainteres()
    {
        this.interesSimple=this.capital*(this.tasaInteresAnual/100)*this.aniosPlazo;
        return this.interesSimple;  
    }
}
