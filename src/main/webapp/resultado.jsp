<%-- 
    Document   : resultado
    Created on : 29-04-2021, 21:14:14
    Author     : catal
--%>


<%@page import="catalinagonzalez.models.CalculadoraInteresModel"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<% CalculadoraInteresModel calculadoraInteres = (CalculadoraInteresModel)request.getAttribute("calculadora"); 
if (calculadoraInteres.getAniosPlazo() == 0){
    request.getRequestDispatcher("index.jsp").forward(request, response);
}
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
         <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>
        <script src="https://kit.fontawesome.com/84fe66ef08.js" crossorigin="anonymous"></script>
    </head>
    <body>
       <div class="container" style="background-color: rgba(179, 209, 255,1); margin: 30px; border-radius: 10px; ">
     
             <h1>Calculadora intereses</h1>
               <div class="row">
                   <div class="col-sm-3">
                       <h3>interes generado <%= "$"+calculadoraInteres.calculainteres() %></h3>
                   </div>
               </div>
               <br/>
               <div class="row">
                   <div class="col-sm-3">
                       <button class="btn btn-primary" onclick="window.location.href='./'"><i class="fas fa-undo-alt"></i> Volver</button>
                   </div>    
               </div>
               <br/>      
 
       </div>  
    </body>
</html>
