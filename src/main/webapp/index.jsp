<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Calculadora intereses</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>
        <script src="https://kit.fontawesome.com/84fe66ef08.js" crossorigin="anonymous"></script>
    </head>
    <body>
      <div class="container" style="background-color: rgba(179, 209, 255,1); margin: 30px; border-radius: 10px; ">
       <form action="CalculadoraInteresController" method="POST" id="interesesForm" name="interesesForm">
          
               <h1>Calculadora intereses</h1>
               <div class="row">
                   <div class="col-sm-3">
                        <label for="capital">Capital</label>
                        <input type="number" class="form-control" name="capital" name="capital" required>
                    </div>
                   <div class="col-sm-3">
                        <label for="anios">Años plazo</label>
                        <input type="number" class="form-control" name="anios" name="anios" required>
                    </div>
                   <div class="col-sm-3">
                        <label for="interesAnual">%Tasa interes anual</label>
                        <input type="number" min="1" max="100" placeholder="% interes" class="form-control" name="interesAnual" name="interesAnual" required>
                    </div>
               </div>
               <br/>
               <div class="row">
                   <div class="col-sm-3">
                        <button type="submit" class="btn btn-success"><i class="fas fa-calculator"></i> Calcular interes</button>
                   </div>    
               </div>
               <br/>
        </form>
       </div>
    </body>
</html>